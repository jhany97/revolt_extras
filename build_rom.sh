#!/bin/bash

# get current path
reldir=`dirname $0`
cd $reldir
DIR=`pwd`

# Colorize and add text parameters
red=$(tput setaf 1)             #  red
grn=$(tput setaf 2)             #  green
cya=$(tput setaf 6)             #  cyan
txtbld=$(tput bold)             # Bold
bldred=${txtbld}$(tput setaf 1) #  red
bldgrn=${txtbld}$(tput setaf 2) #  green
bldylw=${txtbld}$(tput setaf 3) #  yellow
bldblu=${txtbld}$(tput setaf 4) #  blue
bldppl=${txtbld}$(tput setaf 5) #  purple
bldcya=${txtbld}$(tput setaf 6) #  cyan
txtrst=$(tput sgr0)             # Reset

DEVICE="$1"
SYNC="$2"
THREADS="$3"
RELEASE="$4"

# get current version
VERSION=`date +%Y%m%d`

# get time of startup
res1=$(date +%s.%N)

# we don't allow scrollback buffer
echo -e '\0033\0143'
clear

echo -e "${cya}Building ${bldgrn}Re ${bldppl}Volt ${bldblu}ROM ${txtrst}";

# sync with latest sources
echo -e ""
if [ "$SYNC" == "sync" ]
then
   echo -e "${bldblu}Syncing latest ReVolt Sources ${txtrst}"
   repo sync -j"$THREADS"
   echo -e ""
fi

# setup environment
echo -e "${bldblu}Setting up build environment ${txtrst}"
. build/envsetup.sh

# lunch device
echo -e ""
echo -e "${bldblu}Lunching your device ${txtrst}"
lunch "revolt_$DEVICE-userdebug";

echo -e ""
echo -e "${bldblu}Starting to build ReVolt ROM ${txtrst}"

# start compilation
brunch "revolt_$DEVICE-userdebug" -j"$THREADS";
echo -e ""

# finished? get elapsed time
res2=$(date +%s.%N)
echo "${bldgrn}Total time elapsed: ${txtrst}${grn}$(echo "($res2 - $res1) / 60"|bc ) minutes ($(echo "$res2 - $res1"|bc ) seconds) ${txtrst}"

# Let's Make it REVOLT !!
if [ "$RELEASE" == "official" ]
then
    sed -i -e 's/revolt_//' $OUT/system/build.prop
    FINAL=`sed -n -e'/ro.revolt.version/s/^.*=//p' $OUT/system/build.prop`
    echo -e "Let's add the TOUCH !!"
    cd ;
    echo -e "Copying the Files"
    cd /home/johnhany97/Revolt/out/target/product/$DEVICE/ && mkdir tmp;
    mv $FINAL.zip tmp/;
    cd /home/johnhany97/Revolt/out/target/product/$DEVICE/tmp/ && unzip $FINAL.zip;
    echo -e "Copying AROMA Required Files"
    rm -f -r /home/johnhany97/Revolt/out/target/product/$DEVICE/tmp/META-INF/com/google/android/;
    echo -e "Be Patient"
    cp -r /home/johnhany97/Revolt/revolt/$DEVICE/android/ /home/johnhany97/Revolt/out/target/product/$DEVICE/tmp/META-INF/com/google/android/;
    cd ;
    cp -r /home/johnhany97/Revolt/revolt/$DEVICE/Revolt/ /home/johnhany97/Revolt/out/target/product/$DEVICE/tmp/;
    echo -e "Finished Copying the AROMA Files ..."
    cd /home/johnhany97/Revolt/out/target/product/$DEVICE/tmp/ ;
    rm -f $FINAL.zip;
    echo -e "Let's make a Final ZIP for ReVolt ROM"
    zip -r -q ReVolt-JB-$DEVICE-3.0.0.zip *;
    mv /home/johnhany97/Revolt/out/target/product/$DEVICE/tmp/ReVolt-JB-$DEVICE-3.0.0.zip /home/johnhany97/Revolt/ReVoltROM/ROM/FINAL/$DEVICE/;
    echo -e "Let's Upload the official to Goo.IM"
    scp /home/johnhany97/Revolt/out/target/product/$DEVICE/tmp/ReVolt-JB-20130107.zip johnhany97@upload.goo.im:~/public_html/ReVolt_JB_$DEVICE/ ;
    cd ;
    cd /home/johnhany97/Revolt/out/target/product/$DEVICE/ && rm -f -r tmp && rm -f $FINAL.zip;
    cd /home/johnhany97/Revolt/;
    echo -e "DONE !! Enjoy ReVolt JB !!"
else
    echo -e "Let's Upload ReVolt JB Nightly to Goo.IM"
    scp /home/johnhany97/Revolt/out/target/product/$DEVICE/$FINAL.zip johnhany97@upload.goo.im:~/public_html/ReVolt_JB_$DEVICE/Nightlies/ ;
    echo -e "DONE UPLOADING !!"
    echo -e "Release is a Nightly so NO AROMA, Enjoy ReVolt JB"
fi
