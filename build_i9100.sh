#!/bin/bash

# get current path
reldir=`dirname $0`
cd $reldir
DIR=`pwd`

# Colorize and add text parameters
red=$(tput setaf 1)             #  red
grn=$(tput setaf 2)             #  green
cya=$(tput setaf 6)             #  cyan
txtbld=$(tput bold)             # Bold
bldred=${txtbld}$(tput setaf 1) #  red
bldgrn=${txtbld}$(tput setaf 2) #  green
bldylw=${txtbld}$(tput setaf 3) #  yellow
bldblu=${txtbld}$(tput setaf 4) #  blue
bldppl=${txtbld}$(tput setaf 5) #  purple
bldcya=${txtbld}$(tput setaf 6) #  cyan
txtrst=$(tput sgr0)             # Reset

DEVICE="$1"
SYNC="$2"
THREADS="$3"

# get current version
VERSION="$4"

# get time of startup
res1=$(date +%s.%N)

# we don't allow scrollback buffer
echo -e '\0033\0143'
clear

echo -e "${cya}Building ${bldgrn}Re ${bldppl}Volt ${bldblu}ROM ${bldylw}v$VERSION ${txtrst}";

# sync with latest sources
echo -e ""
if [ "$SYNC" == "sync" ]
then
   echo -e "${bldblu}Syncing latest ReVolt Sources ${txtrst}"
   repo sync -j"$THREADS"
   echo -e ""
fi

# setup environment
echo -e "${bldblu}Setting up build environment ${txtrst}"
. build/envsetup.sh

# lunch device
echo -e ""
echo -e "${bldblu}Lunching your device ${txtrst}"
lunch "rootbox_$DEVICE-userdebug";

echo -e ""
echo -e "${bldblu}Starting to build ReVolt ROM ${txtrst}"

# start compilation
brunch "rootbox_$DEVICE-userdebug" -j"$THREADS";
echo -e ""

# finished? get elapsed time
res2=$(date +%s.%N)
echo "${bldgrn}Total time elapsed: ${txtrst}${grn}$(echo "($res2 - $res1) / 60"|bc ) minutes ($(echo "$res2 - $res1"|bc ) seconds) ${txtrst}"

# Let's Make it REVOLT !!
echo -e "Let's add the TOUCH !!"
cd out/target/product/$DEVICE/ && mkdir tmp;
cd out/target/product/$DEVICE/ && mv ReVolt*.zip tmp/;
cd out/target/product/$DEVICE/tmp/ && unzip *.zip;
cd out/target/product/$DEVICE/tmp/ && cp -r revolt/$DEVICE/system/* system/;
cd out/target/product/$DEVICE/tmp/ && cp -r revolt/$DEVICE/Revolt/* Revolt/;
cd out/target/product/$DEVICE/tmp/ && rm -f -r META-INF/com/google/android/;
cd out/target/product/$DEVICE/tmp/ && cp -r revolt/$DEVICE/android/* META-INF/com/google/android/;
cd out/target/product/$DEVICE/tmp/ && rm -f *.zip;
cd out/target/product/$DEVICE/tmp/ && zip -r -q ReVolt-JB-$DEVICE-STABLE_3.0.0_$(date "+%Y%m%d").zip *;
cd out/target/product/$DEVICE/tmp/ && mv ReVolt*.zip /ReVoltROM/ROM/FINAL/$DEVICE/;
cd out/target/product/$DEVICE/ && rm -f -r tmp && rm -f Revolt*;


